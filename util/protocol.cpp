#include "protocol.h"


PROTOCOL protocol;


/*
|--|--|--|----------------|--|--|------------------|--|----|
|0 |1 |2 |       3~10     |11|12|       12+n       |12+n+1|12+n+1+2|

0---------------head_mark-------1byte-----0x7e
1---------------cmd_main--------1byte
2---------------cmd_sub---------1byte
3~10------------imei------------8byte
11--------------item_no---------1byte
12--------------payload_len-----1byte
12+n------------payload---------nbyte
12+n+1----------crc-------------1byte
12+n+1+2--------end-------------2byte-----fixed:0x0d 0x0a
*/


static uint8_t receive_buf[PROTOCOL_BUF_MAX_SIZE]; //接收缓冲
char   receive_last_char      = 0;
int    receive_curr_index     = 0;

static uint8_t common_item_no = 0; //发送信息的编号，最大255后复位
char   protocol_head          = 0x7e;

frame_mark_t send_buf_list; //环形的待发送缓冲区




//构造
PROTOCOL::PROTOCOL()
{
    //初始化相关信息
    send_buf_list.head = 0;
    send_buf_list.tail = 0;
    send_buf_list.lenght = 0;
}


//设置设备IMEI
void PROTOCOL::set_imei(char *imei)
{
    this->imei = imei;
}

//复位接收标志
void PROTOCOL::receive_reset()
{
    receive_last_char = 0;
    receive_curr_index = 0;
}

//接收串口数据
void PROTOCOL::receive_data()
{
	char curr_char;
    curr_char = uart1.receive();
    receive_buf[receive_curr_index++] = curr_char;
    
    if(receive_last_char == 0x0d && curr_char == 0x0a)
    {
        //如果是以\r\n结尾了，则表示这个命令结束了，可以对命令进行解析，并生成需要回复的指令
        //uart1.printf_length((const char*)receive_buf, receive_curr_index);
        
        if(receive_buf[0] == protocol_head && check_sum((char*)receive_buf, receive_curr_index))
        {
            static char payload[PROTOCOL_BUF_MAX_SIZE];
            int payload_len = 0;
            
            if(receive_buf[1] == 0x08 && receive_buf[2] == 0x01)
            {
                payload[payload_len++] = receive_buf[11];
                send_full_cmd(0x02,0x01,payload,payload_len);
            }
            else
            {                
                common_reply(receive_buf[11]);
            }
        }
        
        //复位变量值
        receive_reset();
    }
    else
    {
        receive_last_char = curr_char;
        
        //如果已经到达了缓冲设定最大值，则重置
        if(receive_curr_index >= PROTOCOL_BUF_MAX_SIZE + 16)
        {
            receive_reset();
        }
    }
}

//心跳包
void PROTOCOL::heartbeat()
{
    //{0x7e,0x00,0x01,0x00,0x0d,0x0a};
    //static char str_heart[6] = {0x7e,0x00,0x01,0x00,0x0d,0x0a};
    //return str_heart;
    
    static char payload[PROTOCOL_BUF_MAX_SIZE];
    int payload_len = 0;
    payload[payload_len++] = 0xaa;
    payload[payload_len++] = 0xbb;
    
    send_full_cmd(0x01,0x01,payload,payload_len);
}

//通用回复
void PROTOCOL::common_reply(char item_no)
{
    static char payload[PROTOCOL_BUF_MAX_SIZE];
    int payload_len = 0;
    payload[payload_len++] = item_no;
    
    send_full_cmd(0x02,0x00,payload,payload_len);
}

//向串口发送完整指令
void PROTOCOL::send_full_cmd(char cmd_main,char cmd_sub,char *payload,int payload_len)
{
    common_item_no++;
    if(common_item_no >= 255)
    {
        common_item_no = 1;
    }
    
    int buf_len = 0;
    
    //拼装数据包
    char buf[PROTOCOL_BUF_MAX_SIZE];
    buf[buf_len++] = protocol_head;
    buf[buf_len++] = cmd_main;
    buf[buf_len++] = cmd_sub;
    for(int i = 0; i < 8; i++)
    {
        buf[buf_len++] = this->imei[i];
    }
    buf[buf_len++] = common_item_no;    
    
    buf[buf_len++] = payload_len;
    for(int i = 0; i < payload_len; i++)
    {
        buf[buf_len++] = payload[i];
    }
    
    uint8_t sum = 0;
    for(uint8_t i = 0; i < buf_len; i++)
        sum += buf[i];
    buf[buf_len++] = sum;
    
    buf[buf_len++] = 0x0d;
    buf[buf_len++] = 0x0a;
    //buf[buf_len++] = protocol_head;    
    
    //发送数据
    //uart1.printf_length(buf, buf_len);
    
    //添加到发送队列
    frame_t frame_list;
    frame_list.buf = buf;
    frame_list.buf_len = buf_len;
    write_buf_list(frame_list);
}

//从串口发送数据
void PROTOCOL::cmd_loop()
{
    //no_interrupts(); //原子操作
    
    frame_t rbuf;
    if(read_buf_list(&rbuf))
    {
        uart1.printf_length(rbuf.buf, rbuf.buf_len);
    }
}

//校验指令有效性
bool PROTOCOL::check_sum(char *buf,int buf_len)
{
    return true;
}

//写入buf到队列
bool PROTOCOL::write_buf_list(frame_t frame)
{
    if(send_buf_list.lenght >= PROTOCOL_BUF_LIST_MAX_SIZE) //判断缓冲区是否已满
    {
        return false;
    }
    send_buf_list.frame[send_buf_list.tail] = frame;
    send_buf_list.tail = (send_buf_list.tail+1) % PROTOCOL_BUF_LIST_MAX_SIZE;//防止越界非法访问
    send_buf_list.lenght++;
    return true;
}

//从队列获取一个buf--FIFO
bool PROTOCOL::read_buf_list(frame_t *ret_buf)
{
    if(send_buf_list.lenght == 0)//判断非空
    {
        return false;
    }
    *ret_buf = send_buf_list.frame[send_buf_list.head];//先进先出FIFO，从缓冲区头出
    send_buf_list.head = (send_buf_list.head+1) % PROTOCOL_BUF_LIST_MAX_SIZE;//防止越界非法访问
    send_buf_list.lenght--;
    return true;
}
