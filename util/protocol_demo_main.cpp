/*

*/

//STM32 RUN IN eBox
#include "ebox.h"
#include "protocol.h"

//软件调试串口
//MODE COM1 115200, 0, 8, 1
//ASSIGN COM1 <S1IN> S1OUT


//接收数据
void rx_event()
{
    //char curr_char;
    //curr_char = uart2.receive();
    
    protocol.receive_data();
    
}

//发送数据完成
void tx_complete_event()
{
    //uart1.printf("tx_complete_event...\r\n");
}



void setup()
{
    ebox_init();
    
    //通信用uart2
    uart1.begin(115200);//初始化串口1，波特率：115200
    uart1.attach_rx_interrupt(rx_event);
    //uart2.attach_tx_interrupt(tx_complete_event);
    
    
    //调试信息用uart1
    //uart1.begin(115200);//初始化串口1，波特率：115200
    uart1.printf("this is a serial example\r\n");
}


int count=0;
int main(void)
{
    setup();
    
    char imei[8] = {0x09,0x09,0x08,0x07,0x06,0x05,0x05,0x05};
    //imei = {0x09,0x09,0x08,0x08,0x07,0x06,0x05,0x00};
    protocol.set_imei(imei);
    
    
    /*
    for( int i = 0; i < 6; i++ )
    {
        uart1.put_char(heartBeat[i]);
    }
    */
    
    while(1)
    {
        //uart1.printf("run times = %d\r\n", count);//将count值通过串口输出
        //delay_ms(1000);//延时1s
        
        //uart1.printf("heartBeat sizeof = %d\r\n", sizeof(heartBeat));
        //delay_ms(1000);
        
        protocol.cmd_loop();
        
        if(count >= 100)
        {
            protocol.heartbeat();
            count = 0;
        }
        
        count++;
        delay_ms(10);
    }
    
}




