/*
protocol diy
*/

#ifndef __PROTOCOL_H
#define __PROTOCOL_H

#include "ebox.h"

//�û�����
#define PROTOCOL_BUF_MAX_SIZE 496
#define PROTOCOL_BUF_LIST_MAX_SIZE 10

typedef struct
{
    char * buf;
    int    buf_len;
} frame_t;

typedef struct
{
    uint16_t head;
    uint16_t tail;
    uint16_t lenght;
    frame_t frame[PROTOCOL_BUF_LIST_MAX_SIZE];
} frame_mark_t;

class PROTOCOL
{
    
    
    public:
        PROTOCOL();
        void set_imei(char *imei);
        void heartbeat();
        void receive_data();
        void cmd_loop();
        

    private:
        char * imei;
        void send_full_cmd(char cmd_main,char cmd_sub,char *payload,int payload_len);
        void common_reply(char item_no);
        void receive_reset();
        bool check_sum(char *buf,int buf_len);
        bool write_buf_list(frame_t frame);
        bool read_buf_list(frame_t *ret_buf);
};

extern PROTOCOL protocol;

#endif
