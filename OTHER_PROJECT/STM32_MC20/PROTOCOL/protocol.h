#ifndef __PROTOCOL_H
#define __PROTOCOL_H

#define PROTOCOL_BUF_MAX_SIZE 300
#define PROTOCOL_BUF_LIST_MAX_SIZE 10

typedef struct
{
    char * buf;
    int    buf_len;
} frame_t;

typedef struct
{
    int head;
    int tail;
    int lenght;
    frame_t frame[PROTOCOL_BUF_LIST_MAX_SIZE];
} frame_mark_t;

void protocol_init(char *_imei);
void protocol_heartbeat(void);
void protocol_receive_data(void);
void protocol_cmd_loop(void);

void protocol_send_full_cmd(char cmd_main,char cmd_sub,char *payload,int payload_len);
void protocol_common_reply(char item_no);
void protocol_receive_reset(void);
int protocol_check_sum(char *buf,int buf_len);
int protocol_write_buf_list(frame_t frame);
int protocol_read_buf_list(frame_t *ret_buf);

#endif
