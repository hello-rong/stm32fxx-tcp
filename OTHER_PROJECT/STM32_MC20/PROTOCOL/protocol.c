#include "protocol.h"
#include "M26.h"
#include "stdlib.h"
#include "string.h"

//////////////////////////////////////////////////////////////////////////
/*
|--|--|--|----------------|--|--|------------------|--|----|
|0 |1 |2 |       3~10     |11|12|       12+n       |12+n+1|12+n+1+2|

0---------------head_mark-------1byte-----0x7e
1---------------cmd_main--------1byte
2---------------cmd_sub---------1byte
3~10------------imei------------8byte
11--------------item_no---------1byte
12--------------payload_len-----1byte
12+n------------payload---------nbyte
12+n+1----------crc-------------1byte
12+n+1+2--------end-------------2byte-----fixed:0x0d 0x0a
*/
//////////////////////////////////////////////////////////////////////////  

extern char RxBuffer[300];  
extern int RxCounter;
extern char LongLatistr[100];

static uint8_t receive_buf[300]; //接收缓冲
char   receive_last_char      = 0;
int    receive_curr_index     = 0;
static uint8_t common_item_no = 0; //发送信息的编号，最大255后复位

char * imei;
char   protocol_head = 0x7e;
frame_mark_t send_buf_list; //环形的待发送缓冲区

void protocol_init(char *_imei)
{
    //初始化相关信息
    send_buf_list.head = 0;
    send_buf_list.tail = 0;
    send_buf_list.lenght = 0;
    
    //设置设备IMEI
    imei = _imei;
}

//心跳包
void protocol_heartbeat(void)
{
    //{0x7e,0x00,0x01,0x00,0x0d,0x0a};
    //static char str_heart[6] = {0x7e,0x00,0x01,0x00,0x0d,0x0a};
    //return str_heart;
    
    static char payload[PROTOCOL_BUF_MAX_SIZE];
    int payload_len = 0;
    int i;
    
    GetLongLatistr();    
    
    //payload[payload_len++] = 0xaa;
    //payload[payload_len++] = 0xbb;
    
    for(i = 0; i < strlen(LongLatistr); i++)
    {
        if(LongLatistr[i] == 0x5e)
        {
            break;
        }
        
        payload[payload_len++] = LongLatistr[i];
    }
    
    protocol_send_full_cmd(0x01,0x01,payload,payload_len);
}

//接收串口数据并处理
void protocol_receive_data(void)
{    
    int i;
    int cmd_flag = 0;
    char curr_char;
    
    if(RxCounter == 0)
        return;    
    
    for(i = 0; i < RxCounter; i++)
    {
        curr_char = RxBuffer[i];
        if(curr_char == protocol_head)
        {
            cmd_flag = 1;
        }
        
        if(cmd_flag)
        {
            receive_buf[receive_curr_index++] = curr_char;
            
            if(receive_last_char == 0x0d && curr_char == 0x0a)
            {
                Uart1_SendStr( (char *)receive_buf );
                Uart1_SendStr( (char *)"\r\n" );
                
                //指令已接收成功，开始解析指令
                //...
                if(receive_curr_index > 10 && receive_buf[0] == protocol_head && protocol_check_sum((char*)receive_buf, receive_curr_index))
                {
                    static char payload[PROTOCOL_BUF_MAX_SIZE];
                    int payload_len = 0;
                    
                    //处理示例...
                    if(receive_buf[1] == 0x08 && receive_buf[2] == 0x01)
                    {
                        payload[payload_len++] = receive_buf[11];
                        protocol_send_full_cmd(0x02,0x01,payload,payload_len);
                    }
                    else
                    {                
                        //common_reply(receive_buf[11]);
                    }
                }
                
                
                //复位变量值
                protocol_receive_reset();
            }
            else
            {
                receive_last_char = curr_char;
                
                //如果已经到达了缓冲设定最大值，则重置
                if(receive_curr_index >= PROTOCOL_BUF_MAX_SIZE + 16)
                {
                    protocol_receive_reset();
                }
            }
        }
    }
}

void protocol_cmd_loop(void)
{
    frame_t rbuf;
    
    //no_interrupts(); //原子操作
    
    protocol_receive_data();    
    
    if(protocol_read_buf_list(&rbuf))
    {
        //uart1.printf_length(rbuf.buf, rbuf.buf_len);
        Send_Str(rbuf.buf);
    }
}


void protocol_send_full_cmd(char cmd_main,char cmd_sub,char *payload,int payload_len)
{
    int i;
    int buf_len = 0;
    uint8_t sum = 0;
    char buf[PROTOCOL_BUF_MAX_SIZE];
    frame_t frame_list;
    
    common_item_no++;
    if(common_item_no >= 255)
    {
        common_item_no = 1;
    }
    
    //拼装数据包    
    buf[buf_len++] = protocol_head;
    buf[buf_len++] = cmd_main;
    buf[buf_len++] = cmd_sub;
    for(i = 0; i < 8; i++)
    {
        buf[buf_len++] = imei[i];
    }
    buf[buf_len++] = common_item_no;    
    
    buf[buf_len++] = payload_len;
    for(i = 0; i < payload_len; i++)
    {
        buf[buf_len++] = payload[i];
    }
    
    
    for(i = 0; i < buf_len; i++)
        sum += buf[i];
    buf[buf_len++] = sum;
    
    buf[buf_len++] = 0x0d;
    buf[buf_len++] = 0x0a;
    //buf[buf_len++] = protocol_head;    
    
    //发送数据
    //uart1.printf_length(buf, buf_len);
    
    //添加到发送队列    
    frame_list.buf = buf;
    frame_list.buf_len = buf_len;
    protocol_write_buf_list(frame_list);
}

void protocol_common_reply(char item_no)
{
    static char payload[PROTOCOL_BUF_MAX_SIZE];
    int payload_len = 0;
    payload[payload_len++] = item_no;
    
    protocol_send_full_cmd(0x02,0x00,payload,payload_len);
}

//复位接收标志
void protocol_receive_reset(void)
{
    receive_last_char = 0;
    receive_curr_index = 0;
    Clear_Buffer();
}


int protocol_check_sum(char *buf,int buf_len)
{
    return 1;
}

int protocol_write_buf_list(frame_t frame)
{
    if(send_buf_list.lenght >= PROTOCOL_BUF_LIST_MAX_SIZE) //判断缓冲区是否已满
    {
        return 0;
    }
    send_buf_list.frame[send_buf_list.tail] = frame;
    send_buf_list.tail = (send_buf_list.tail+1) % PROTOCOL_BUF_LIST_MAX_SIZE;//防止越界非法访问
    send_buf_list.lenght++;
    return 1;
}

int protocol_read_buf_list(frame_t *ret_buf)
{
    if(send_buf_list.lenght == 0)//判断非空
    {
        return 0;
    }
    *ret_buf = send_buf_list.frame[send_buf_list.head];//先进先出FIFO，从缓冲区头出
    send_buf_list.head = (send_buf_list.head+1) % PROTOCOL_BUF_LIST_MAX_SIZE;//防止越界非法访问
    send_buf_list.lenght--;
    return 1;
}
