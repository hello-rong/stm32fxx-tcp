#include <stm32f10x.h>
#include "string.h"  
#include "hex_str.h"  
int mypow(int x,int n)
{
	    int t=1;
    if(n==0) return 1;

    for(;n>0;t*=x,n--);
    return t;
}

unsigned char hex_check(char* s)
{
    unsigned char result=1;
    for(;*s!='\0';s++)
    {
        if(*s<'0' || (*s>'9' && *s<'A') || (*s>'F' && *s<'a') || *s>'f') 
        {
            result=0;
            break;
        }
    }
    return result;
}
int conv(char* s)
{
    int t=0;
    int i=0;
    if(hex_check(s))
    {
        int ln=(int)strlen(s)-1;
        for(;ln>=0;ln--,i++)
        {
            if(s[ln]>='0' && s[ln]<='9') t+=(s[ln]-0x30)*mypow(16,i);
            if(s[ln]>='a' && s[ln]<='f') t+=(s[ln]-0x57)*mypow(16,i);
            if(s[ln]>='A' && s[ln]<='F') t+=(s[ln]-0x37)*mypow(16,i);
        }
    }
    else return -1;
    return t;
}