#include "led.h"
#include "delay.h"
#include "sys.h"
#include "usart.h"	 
#include "math.h"			
#include "stdio.h"
#include "stm32f10x_flash.h"
#include "stdlib.h"
#include "string.h"
#include "wdg.h"
#include "m26.h"
#include "protocol.h"

//#include <http.h>
extern char LongLatistr[100];//经纬度数据字符串
extern char RxBuffer[300];  
extern int RxCounter;

int main(void)
{	
    int icount = 0; //计数器
    delay_init();	    	 //延时函数初始化	  
    NVIC_Configuration(); 	 //设置NVIC中断分组2:2位抢占优先级，2位响应优先级
    LED_Init();		  		//初始化与LED连接的硬件接口

    uart_init(9600);//串口1初始化，可连接PC进行打印模块返回数据
    uart2_init(115200);//初始化和GPRS连接串口

    Uart1_SendStr( (char *)"uart-init-OK...\r\n" );

    //	 IWDG_Init(7,625);    //8S一次
    Uart1_SendStr( (char *)"open-mc20...\r\n" );
    OPEN_MC20();

    Uart1_SendStr( (char *)"GSM_Init...\r\n" );
    GSM_Init(); //GPRS初始化

    Uart1_SendStr( (char *)"Get_LBSStationRes...\r\n" );
    Get_LBSStationRes();

    //Uart1_SendStr( (char *)"HTTP_Get...\r\n" );
    //HTTP_Get();

    //Uart1_SendStr( (char *)"BT_Init...\r\n" );
    //BT_Init(); //BT初始化

    Uart1_SendStr( (char *)"TCP_AUTH...\r\n" );
    Send_Str((char *)"TCP-AUTH-PWD-123456");
    Clear_Buffer();
     

    delay_ms(200);
    IWDG_Feed();//喂狗

    while(1)
    {
        Uart1_SendStr( (char *)"WHILE...\r\n" );	
        //LongLatistr[0]='S';
        //	LongLatistr[1]=':';	
        //	LongLatistr[25]=0;
        //  Send_Str(LongLatistr);//发送经纬度数据

        icount++;

        if(icount >= 60)
        {
            Uart1_SendStr( (char *)"POST_LongLatistr...\r\n" );
            Send_Str((char *)"");//发送经纬度数据
            icount = 0;
        }


        //BT_Connect();

        delay_ms(1000);

        LED=!LED;
        IWDG_Feed();//喂狗

    }
}






