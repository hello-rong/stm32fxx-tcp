#include "M26.h"
#include "led.h"
#include "usart.h"	 
#include "stdlib.h"
#include "string.h"
#include "wdg.h"
#include "delay.h"
#include "hex_str.h"  
char *strx=0,*Readystrx,*Errstrx; 	//返回值指针判断
char LongLatistr[100];//经纬度数据字符串
extern char  RxBuffer[300];
extern int RxCounter;
M26_LBS lbs;
void Uart1_SendStr(char*SendBuf)//串口1打印数据
{
	while(*SendBuf)
	{
        while((USART1->SR&0X40)==0);//等待发送完成 
        USART1->DR = (u8) *SendBuf; 
        SendBuf++;
	}
}
void Clear_Buffer(void)//清空缓存
{
		u8 i;
		Uart1_SendStr(RxBuffer);
		for(i=0;i<RxCounter;i++)
		RxBuffer[i]=0;//缓存
		RxCounter=0;
		IWDG_Feed();//喂狗
}

void OPEN_MC20(void)
{
	char *strx;
		printf("AT\r\n"); 
		delay_ms(300);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//返回OK
		printf("AT\r\n"); 
		delay_ms(300);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//返回OK
	 IWDG_Feed();//喂狗
	if(strx==NULL)
	{
	PWRKEY=1;//拉低
	delay_ms(300);
	delay_ms(300);
	PWRKEY=0;//拉高正常开机
   IWDG_Feed();//喂狗
	}
	printf("AT\r\n"); 
		delay_ms(300);
	 IWDG_Feed();//喂狗
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//返回OK
		while(strx==NULL)
		{
				Clear_Buffer();	
				printf("AT\r\n"); 
				delay_ms(300);
			LED=!LED;
				strx=strstr((const char*)RxBuffer,(const char*)"OK");//返回OK
		}
		LED=0;
		 IWDG_Feed();//喂狗
		//printf("ATE0&W\r\n"); 
				delay_ms(300);
				Clear_Buffer();
}

void  GSM_Init(void)
{
		int i;
	
		printf("AT\r\n"); 
		delay_ms(500);
		printf("AT\r\n"); 
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//返回OK
		while(strx==NULL)
		{
				Clear_Buffer();	
				printf("AT\r\n"); 
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"OK");//返回OK
		}
		printf("ATE1\r\n"); //打开回显
		delay_ms(500);
		Clear_Buffer();	
		printf("AT+CSQ\r\n"); //检查CSQ，可以利用手机APP查看CSQ值
		delay_ms(500);
		/////////////////////////////////
		printf("AT+CPIN?\r\n");//检查SIM卡是否在位
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"+CPIN: READY");//查看是否返回ready
		while(strx==NULL)
		{
				Clear_Buffer();
				printf("AT+CPIN?\r\n");
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"+CPIN: READY");//检查SIM卡是否在位，等待卡在位，如果卡识别不到，剩余的工作就没法做了
		}
		Clear_Buffer();	
		///////////////////////////////////
		printf(" AT+CREG?\r\n");//查看是否注册GSM网络
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"+CREG: 0,1");//返回正常
		while(strx==NULL)
		{
				Clear_Buffer();
				printf(" AT+CREG?\r\n");//查看是否注册GSM网络
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"+CREG: 0,1");//返回正常
		}
		Clear_Buffer();
		/////////////////////////////////////
		printf(" AT+CGREG?\r\n");//查看是否注册GPRS网络
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"+CGREG: 0,1");//，这里重要，只有注册成功，才可以进行GPRS数据传输。
		while(strx==NULL)
		{
				Clear_Buffer();
				printf(" AT+CGREG?\r\n");//查看是否注册GPRS网络
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"+CGREG: 0,1");//，这里重要，只有注册成功，才可以进行GPRS数据传输。
		}
		Clear_Buffer();
			printf("AT+QIclose\r\n");//关闭移动场景，意思就是关闭相关的数据连接
		delay_ms(500);
		printf("AT+QIDEACT\r\n");//关闭移动场景，意思就是关闭相关的数据连接
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//查看是否成功关闭
		while(strx==NULL)//关闭失败，继续关闭
		{
				Clear_Buffer();
				printf("AT+QIDEACT\r\n");
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"OK");//关闭成功
		}
		Clear_Buffer();
		printf("AT+QIFGCNT=0\r\n");//配置场景，为后面的数据连接做基础
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//开启成功
		while(strx==NULL)
		{
				Clear_Buffer();
				printf("AT+QIFGCNT=0\r\n");
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"OK");//开启成功
		}
		Clear_Buffer();
		printf("AT+QICSGP=1,\042CMNET\042\r\n");//接入APN，
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//开启成功
		while(strx==NULL)
		{
				Clear_Buffer();
				printf("AT+QICSGP=1,\042CMNET\042\r\n");//接入APN，
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"OK");////开启成功
		}
		Clear_Buffer();
		printf("AT+QIREGAPP\r\n");//注册TCP协议栈
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//开启成功
		while(strx==NULL)
		{
				Clear_Buffer();
				printf("AT+QIREGAPP\r\n");//注册TCP协议栈
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"OK");//开启成功
		}
		Clear_Buffer();
		printf("AT+QISTAT\r\n");//查询当前连接状态
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"IP START"); //判断当前IP状态,只有变为start，才能激活PDP
		while(strx==NULL)
		{
				Clear_Buffer();
				printf("AT+QISTAT\r\n");//查询当前连接状态
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"IP START");//开启成功
		}
		printf("AT+QIACT\r\n");//PDP上下文激活
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//开启成功
		while(strx==NULL)
		{
				Clear_Buffer();
				printf("AT+QIACT\r\n");//PDP激活
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"OK");//开启成功
		}
		Clear_Buffer();
        
        
		printf("AT+QIDNSIP=0\r\n");//使用IP连接还是域名连接，0是IP 1是域名
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//设置ok
		while(strx==NULL)
		{
				Clear_Buffer();
				printf("AT+QIDNSIP=0\r\n");
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"OK");//设置ok
		}
		Clear_Buffer();
		printf("AT+QIOPEN=\042TCP\042,\042106.14.211.46\042,\0428580\042\r\n");//这里是需要登陆的IP号码，输入公网的IP以及端口号就可以了。
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"CONNECT OK");//判断连接状态
		
		Readystrx=strstr((const char*)RxBuffer,(const char*)"ALREADY CONNECT"); //已经连接状态
		while(strx==NULL&&Readystrx==NULL)
		{
				strx=strstr((const char*)RxBuffer,(const char*)"CONNECT OK");//判断连接状态
				Readystrx=strstr((const char*)RxBuffer,(const char*)"ALREADY CONNECT"); //已经连接状态
				delay_ms(100);
				IWDG_Feed();//喂狗
		}
		delay_ms(500);
		delay_ms(500);
		IWDG_Feed();//喂狗
		Clear_Buffer();
	
}		

//获取基站原始数据
void Get_LBSStationRes(void)
{
	u8 i=0,j=0;
	printf("AT+CGREG=2\r\n");//
	delay_ms(300);
	strx=strstr((const char*)RxBuffer,(const char*)"OK");//开启成功
		while(strx==NULL)
		{
		  strx=strstr((const char*)RxBuffer,(const char*)"OK");//开启成功
		}
		Clear_Buffer();
	printf("AT+CGREG?\r\n");//获取基站原始数据
	delay_ms(300);
	strx=strstr((const char*)RxBuffer,(const char*)"OK");//获取成功
		while(strx==NULL)
		{
		  strx=strstr((const char*)RxBuffer,(const char*)"OK");//获取成功
		}
			strx=strstr((const char*)RxBuffer,(const char*)"+CGREG:");//获取成功
		if(strx)
		{
				strx=strstr((const char*)RxBuffer,(const char*)"\042");//查询第一个双引号
			if(strx)
			{
				i=0;j=0;
				while(1)
				{
					if(strx[i+1]=='\042')//等于双引号跳出
					{
						lbs.lac[j]=0;
						 break;//
					}
    			lbs.lac[j++]=strx[i+1];//获取lac数据
					i++;
		    }
		  }
		  strx=strstr((const char*)(strx+i+2),(const char*)"\042");//指针移动位置查询第三个双引号
				if(strx)
			{
				i=0;j=0;
				while(1)
				{
					if(strx[i+1]=='\042')//等于双引号跳出
					{
						lbs.cid[j]=0;
						 break;//
					}
    			lbs.cid[j++]=strx[i+1];//获取lac数据
					i++;
		    }
		  }
			
    }
		Clear_Buffer();	
		lbs.lacdata=conv(lbs.lac);
  sprintf(lbs.lac,"%d",lbs.lacdata);	
	lbs.ciddata=conv(lbs.cid);
  sprintf(lbs.cid,"%d",	lbs.ciddata);
}



//获取GPS定位
void GetLongLatistr(void)
{
    int trycount = 300;
    u8 i = 0;
    
    //delay_ms(500);
    printf("AT+QCELLLOC=1\r\n");//获取经纬度数据
    delay_ms(500);
    strx=strstr((const char*)RxBuffer,(const char*)"OK");//获取经纬度
    Errstrx=strstr((const char*)RxBuffer,(const char*)"+CME ERROR");//获取到了错误的指令代码
    while(Errstrx);//选择复位MCU，模块数据读取异常
    while(strx==NULL)
    {
        Clear_Buffer();
        Uart1_SendStr( (char *)"WHILE_QCELLLOC...\r\n" );
        printf("AT+QCELLLOC=1\r\n");
        delay_ms(500);
        strx=strstr((const char*)RxBuffer,(const char*)"OK");//获取经纬度

        trycount--;
        if(trycount <= 0)
        {                
            Uart1_SendStr( (char *)"QCELLLOC_FAIL...\r\n" );
            //Clear_Buffer();
            strx = 0;
        }
    }
    
    //Uart1_SendStr( (char *)"QCELLLOC_FAIL_P2...\r\n" );
    
    if(trycount > 0)
    {
        strx=strstr((const char*)RxBuffer,(const char*)":");//返回经纬度位置
        if(strx)
        {
            for(i=0;;i++)
            {
                if(strx[i+2]!=0x0D)//???????,,????????
                    LongLatistr[i]=strx[i+2];
                else
                {
                    //  Sendnum=i;


                    break;//????
                }

            }            
            
        }
    }
    
    LongLatistr[i] = 0x5e; //用0x5e（^）来标识结束
    
    Uart1_SendStr( (char *)strcat("LongLatistr:", LongLatistr) );
    Uart1_SendStr( (char *)"\r\n");
}

void Send_Str(char*data)
{
    //u8 i;    
    u8 untildata=0xff;//发送剩余字节数
    
    //printf("ATE0\r\n"); //关闭回显
    Clear_Buffer();
    
    if(strlen(data) <= 0)
    {
        Uart1_SendStr( (char *)"Send_Str(EMPTY)...\r\n" );
        return;
        //GetLongLatistr();
        //data = LongLatistr;
    }
    
    /*
    if(strlen(data) <= 0)
    {
        Uart1_SendStr( (char *)"QCELLLOC_NONE...\r\n" );
        data = "NONE";
    }
    */
    
    Clear_Buffer();	
    printf("AT+QISEND\r\n");//发送函数
    delay_ms(100);
    printf(data);
    delay_ms(100);	
    USART_SendData(USART2, (u8) 0x1a);//发送完成函数
    while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET)
    {
    }
    delay_ms(100);
    strx=strstr((char*)RxBuffer,(char*)"SEND OK");//是否正确发送
    while(strx==NULL)
    {
            strx=strstr((char*)RxBuffer,(char*)"SEND OK");//是否正确发送
            delay_ms(10);
    }
    delay_ms(100);

    /*
    Clear_Buffer();
    printf("AT+QISACK\r\n");
    strx=strstr((char*)RxBuffer,(char*)"+QISACK");//发送剩余字节数据
    while(untildata)
    {
        printf("AT+QISACK\r\n");
    strx=strstr((char*)RxBuffer,(char*)"+QISACK");//发送剩余字节数据
            strx=strstr((char*)RxBuffer,(char*)"+QISACK");//发送剩余字节数据
            strx=strstr((char*)strx,(char*)",");//获取第一个,
            strx=strstr((char*)(strx+1),(char*)",");//获取第二个,
          untildata=*(strx+2)-0x30;
        delay_ms(100);
         IWDG_Feed();//喂狗
    }
    */

    Clear_Buffer();
    IWDG_Feed();//喂狗

}

void BT_Init(void)
{
		int i;
	
		printf("AT\r\n"); 
		delay_ms(500);
		printf("AT\r\n"); 
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//返回OK
		while(strx==NULL)
		{
				Clear_Buffer();	
				printf("AT\r\n"); 
				delay_ms(500);
				strx=strstr((const char*)RxBuffer,(const char*)"OK");//返回OK
		}
        printf("ATE1\r\n"); //打开回显
	///////////////////////////////////
		printf("at+qbtpwr=1\r\n");//打开蓝牙
		delay_ms(500);
		strx=strstr((const char*)RxBuffer,(const char*)"OK");//等待打开OK
	
		printf("AT+QBTNAME=\042MC20_BT\042\r\n");
		delay_ms(500);
		printf("AT+QBTVISB=1\r\n");//永久发现
	  delay_ms(500);
		
		
}

unsigned char RecID;
unsigned char getdata[3];
void BT_Connect(void)
{
	
	strx=strstr((const char*)RxBuffer,(const char*)"+QBTIND: \042pair\042");//获取到有连接对象
	if(strx)
	{
		printf("AT+QBTPAIRCNF=1\r\n");//允许连接
			  delay_ms(500);
		   Clear_Buffer();	
  }
	strx=strstr((const char*)RxBuffer,(const char*)"+QBTIND: \042conn\042");//密码配对ok
	if(strx)
	{
		printf("AT+QBTACPT=1\r\n");//正式连接
			  delay_ms(500);
				Clear_Buffer();	
  }
	strx=strstr((const char*)RxBuffer,(const char*)"+QBTACPT:");//连接成功返回
	if(strx)
				RxCounter=0;
		strx=strstr((const char*)RxBuffer,(const char*)"recv");//读取数据
	if(strx)
	{
		RecID=strx[6];
		Clear_Buffer();	
		printf("AT+QSPPREAD=%c,1500\r\n",RecID);
		 delay_ms(200);
			strx=strstr((const char*)RxBuffer,(const char*)"QSPPREAD");//读取数据
		if(strx)
		{
				getdata[0]=strx[13];
				getdata[1]=strx[14];
				getdata[2]=strx[15];
		} 
		RxCounter=0;
	}
	
	if(RecID)
	{
   printf("AT+QSPPSEND=%c,10\r\n",RecID);
	  delay_ms(100);
	printf("Send1234\r\n");
	  delay_ms(500);
	 	RxCounter=0;
	}
	
}

