#ifndef __M26_H
#define __M26_H	
#include "stm32f10x.h"
void Uart1_SendStr(char*SendBuf);//串口1打印数据;
void Clear_Buffer(void);//清空缓存
void OPEN_MC20(void);
void GSM_Init(void);//M26初始化
void Send_Str(char*data);//发送数据
void Get_LBSStationRes(void);
void BT_Init(void);//BT初始化
void BT_Connect(void);
void GetLongLatistr(void);
typedef struct
{
   uint8_t lac[10];    
	 uint16_t lacdata;
	 uint8_t cid[10];  
	 uint16_t ciddata;

} M26_LBS;

#endif  

